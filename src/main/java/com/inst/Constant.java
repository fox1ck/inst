package com.inst;

final class Constant {
  private Constant() {
  }

  static final String INST_LOGIN_URL = "https://www.instagram.com/accounts/login/";
  static final String INST_FOLLOWERS_URL = "https://www.instagram.com/graphql/query/?query_id=17851374694183129";
}
