package com.inst.proxy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
@Slf4j
public class PaidProxyService {

  private static final Set<String> stringStream = Stream.of(
          "213.155.9.129",
          "213.155.9.130",
          "213.155.9.131",
          "213.155.9.132",
          "213.155.9.133",
          "213.155.9.134",
          "213.155.9.135",
          "213.155.9.136",
          "213.155.9.137",
          "213.155.9.138",
          "213.155.9.139",
          "213.155.9.140",
          "213.155.9.141",
          "213.155.9.142",
          "213.155.9.143",
          "213.155.9.144",
          "213.155.9.145",
          "213.155.9.146",
          "213.155.9.147",
          "213.155.9.148",
          "213.155.9.149",
          "213.155.9.150",
          "213.155.9.151"
  ).collect(Collectors.toSet());


  private BlockingQueue<String> queue = new LinkedBlockingQueue<>();

  public Proxy getWorkingProxy() {
    if (queue.isEmpty()) {
      queue.addAll(stringStream);
    }
    String randomProxyIp = null;
    try {
      randomProxyIp = queue.take();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return Proxy.builder()
            .proxyType(Proxy.ProxyType.HTTP)
            .port(7384)
            .ip(randomProxyIp)
            .auth(new Proxy.Auth("ua70726", "9DPJ1drfeX")).build();
  }
}
