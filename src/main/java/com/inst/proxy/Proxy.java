package com.inst.proxy;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Proxy {
  private String ip;
  private Integer port;
  private ProxyType proxyType;
  private Auth auth;

  public enum ProxyType {
    HTTP, HTTPS, SOCKS5
  }

  @Data
  public static class Auth {
    private final String userName;
    private final String password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;

    Proxy proxy = (Proxy) o;

    if (!ip.equals(proxy.ip)) return false;
    if (!port.equals(proxy.port)) return false;
    return proxyType == proxy.proxyType;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + ip.hashCode();
    result = 31 * result + port.hashCode();
    result = 31 * result + proxyType.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return String.format("%s:%s:%s", ip, port, proxyType);
  }

}
