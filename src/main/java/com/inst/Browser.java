package com.inst;

import com.inst.proxy.Proxy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;

@Component
@Scope("prototype")
@Slf4j
public class Browser {

  public Browser(Proxy proxy) {
    this.proxy = proxy;
  }

  public PhantomJSDriver getDriver() {
    return driver;
  }

  private PhantomJSDriver driver;

  private final Proxy proxy;

  @PostConstruct
  private void init() {
    log.debug("Browser initialization");
    String[] phantomArgs = new String[]{
            "--webdriver-loglevel=NONE",
            String.format("--proxy=%s:%s", proxy.getIp(), proxy.getPort()),
            "--proxy-type=http",
            String.format("--proxy-auth=%s:%s", proxy.getAuth().getUserName(), proxy.getAuth().getPassword())};
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
            phantomArgs);
    final PhantomJSDriverService defaultService =
            PhantomJSDriverService.createDefaultService(capabilities);
    driver = new PhantomJSDriver(defaultService,
            capabilities);
    driver.setLogLevel(Level.OFF);
  }

  public void makeScr() {
    File scrFile = driver.getScreenshotAs(OutputType.FILE);
    try {
      final String fileName =
              String.format("D:/%s.png",
                      System.nanoTime());
      File destFile =
              new File(fileName);
      destFile.getParentFile().mkdirs();
      FileUtils.copyFile(scrFile, destFile, true);
      final File file = new File(fileName + ".txt");
      try (FileOutputStream fos = new FileOutputStream(file)) {
        fos.write(driver.getPageSource().getBytes("UTF-8"));
        fos.flush();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
