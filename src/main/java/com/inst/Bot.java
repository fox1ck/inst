package com.inst;

import com.google.common.base.Predicate;
import com.google.gson.Gson;
import com.inst.resp.FollowersResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Scope("prototype")
@RequiredArgsConstructor
public class Bot {

  private final String userName;
  private final String password;
  private final Browser browser;

  public void login() {
    browser.getDriver().get(Constant.INST_LOGIN_URL);
    WebDriverWait webDriverWait = new WebDriverWait(browser.getDriver(), 15);
    webDriverWait.until((Predicate<WebDriver>) webDriver -> webDriver.findElement(By.name("username")).isDisplayed());
    browser.getDriver().findElementByName("username").sendKeys(userName);
    webDriverWait.until((Predicate<WebDriver>) webDriver -> webDriver.findElement(By.name("password")).isDisplayed());
    browser.getDriver().findElementByName("password").sendKeys(password);
    browser.getDriver().findElementByCssSelector("button:first-of-type").click();
    webDriverWait.until((Predicate<WebDriver>) input -> input.findElement(By.cssSelector("input[placeholder=Пошук]")).isEnabled());
  }

  public FollowersResponse fetchNextFollowers(int count, Long userId, String endCursor) {
    FollowersResponse followersResponse = null;
    try {
      String requestUrl = String.format("%s&id=%s&first=%s%s", Constant.INST_FOLLOWERS_URL, userId, count, endCursor == null ? "" : "&after=" + endCursor);
      browser.getDriver().get(requestUrl);
      String text = Jsoup.parse(browser.getDriver().getPageSource()).body().text();
      followersResponse = new Gson().fromJson(text, FollowersResponse.class);
    } catch (Exception e) {
      System.out.println("*********************");
      System.out.println(Jsoup.parse(browser.getDriver().getPageSource()).body().text());
      System.out.println("*********************");
    }
    return followersResponse;
  }
}
