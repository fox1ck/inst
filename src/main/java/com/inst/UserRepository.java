package com.inst;

import com.inst.resp.NodeObj;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<NodeObj, String> {

  NodeObj findByUserId(String id);
}

