package com.inst;

import com.inst.proxy.PaidProxyService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BotFactory {

  private final ApplicationContext applicationContext;
  private final PaidProxyService paidProxyService;

  public Bot create(String userName, String password) {
    return applicationContext.getBean(Bot.class, userName, password, applicationContext.getBean(Browser.class, paidProxyService.getWorkingProxy()));
  }
}
