package com.inst.resp;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class UserObj {

  @SerializedName("edge_followed_by")
  private EdgeFollowedByObj edgeFollowedBy;
}
