package com.inst.resp;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class PageInfoObj {

  @SerializedName("has_next_page")
  private Boolean hasNextPage;

  @SerializedName("end_cursor")
  private String endCursor;
}
