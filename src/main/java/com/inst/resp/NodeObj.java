package com.inst.resp;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "users")
public class NodeObj {

  @Id
  private String _id;

  @SerializedName("id")
  private String userId;

  @SerializedName("username")
  private String userName;

  @SerializedName("full_name")
  private String fullName;

}
