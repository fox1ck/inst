package com.inst.resp;

import lombok.Data;

@Data
public class FollowersResponse {

  private String status;
  private DataObj data;
}
