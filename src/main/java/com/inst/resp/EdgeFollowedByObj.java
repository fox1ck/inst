package com.inst.resp;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class EdgeFollowedByObj {
  private Integer count;

  @SerializedName("page_info")
  private PageInfoObj pageInfo;

  private List<EdgeObj> edges;
}
