package com.inst.resp;

import lombok.Data;

@Data
public class EdgeObj {
  private NodeObj node;
}
