package com.inst;

import com.inst.resp.EdgeObj;
import com.inst.resp.FollowersResponse;
import com.inst.resp.NodeObj;
import com.inst.resp.PageInfoObj;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BotTest {

  @Autowired
  private BotFactory factory;

  @Autowired
  private UserRepository userRepository;

  private List<Bot> bots = new ArrayList<>();

  private Map.Entry<String, String>[] temp = new Map.Entry[]
          {
                  new AbstractMap.SimpleEntry("test_karina_b", "Fox1ck345121994"),
                  new AbstractMap.SimpleEntry("dalinaredaktor", "Fox1ck345121994"),
          };

  private int botIndex;

  @Test
  public void export_to_file() throws IOException {
    int perPage = 40000;
    int currentPage = 0;
    Page<NodeObj> res;
    try (FileWriter wr = new FileWriter("followers.csv")) {
      do {
        res = userRepository.findAll(PageRequest.of(currentPage, perPage));
        for (NodeObj nodeObj : res.getContent()) {
          wr.write(nodeObj.getUserId() + ";" + nodeObj.getUserName() + "\r\n");
        }
        currentPage++;
        wr.flush();
      } while (!res.isLast());
    }

  }

  @Test
  public void test() {
    for (Map.Entry<String, String> entry : temp) {
      Bot e = factory.create(entry.getKey(), entry.getValue());
      e.login();
      bots.add(e);
    }

    String nextCursor = null;
    Boolean hasNext = true;
    Integer cnt = 5000;
    do {
      if (cnt < 2000) {
        cnt = 4000;
      }
      FollowersResponse nextFollowers = chooseNextBot().fetchNextFollowers(cnt, 20315007L, nextCursor);
      if (nextFollowers != null) {
        if (nextFollowers.getStatus().equals("ok")) {
          for (EdgeObj edgeObj : nextFollowers.getData().getUser().getEdgeFollowedBy().getEdges()) {
            NodeObj node = edgeObj.getNode();
            NodeObj byUserId = userRepository.findByUserId(node.getUserId());
            if (byUserId == null) {
              userRepository.save(node);
            }
          }
          PageInfoObj pageInfo = nextFollowers.getData().getUser().getEdgeFollowedBy().getPageInfo();
          nextCursor = pageInfo.getEndCursor();
          hasNext = pageInfo.getHasNextPage();
        } else {
          if (botIndex == bots.size() - 1) {
            sleep(1000 * 60 * 15);
          }
          chooseNextBot();
        }
      } else {
        cnt--;
        sleep(1000 * 60 * 2);
      }
      sleep(5000);
    } while (hasNext);
  }

  private Bot chooseNextBot() {
    Bot res;
    if (botIndex == 0 || botIndex == bots.size() - 1) {
      botIndex = 0;
    }
    res = bots.get(botIndex);
    botIndex++;
    return res;
  }

  private void sleep(long duration) {
    try {
      System.out.println("Sleep " + duration);
      Thread.sleep(duration);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}